import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'MarvelAPP',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
