import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule} from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { NgIf } from '@angular/common';


@Component({
  selector: 'app-characterprofile',
  templateUrl: './characterprofile.page.html',
  styleUrls: ['./characterprofile.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule, RouterModule,NgbCarouselModule,NgIf]
})
export class CharacterprofilePage implements OnInit {

  profileId: any;
  profile: any;
  characters: any = [];
  characterDesc:string = '';

  comics: any = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) { }

  ngOnInit() {
    
    this.profileId = this.activatedRoute.snapshot.paramMap.get('id');

    this.http.get<any>('https://gateway.marvel.com:443/v1/public/characters/' + this.profileId + '?ts=1&apikey=02c250531a086a4ec9f85901e468bd06&hash=f111df853742da4151eb900421fdcc73').subscribe(res => {

      this.profile = res.data.results[0];

      if(this.profile.description === ''){

        this.characterDesc = 'No description';

      }else{

        this.characterDesc = this.profile.description;

      }

      for(let i = 0; i < this.profile.comics.items.length; i++){
        this.http.get<any>(this.profile.comics.items[i].resourceURI + '?ts=1&apikey=02c250531a086a4ec9f85901e468bd06&hash=f111df853742da4151eb900421fdcc73').subscribe(res => {
  
          this.comics.push(...res.data.results);
  
        })
      }

    })

  }

  infoCharacterClic(){
    
    let characterInfoURL = this.profile.urls[0].url;

    window.open(characterInfoURL, '_blank');

  }


}
