import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { InfiniteScrollCustomEvent } from '@ionic/angular';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-characterlist',
  templateUrl: './characterlist.page.html',
  styleUrls: ['./characterlist.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule, RouterModule, Ng2SearchPipeModule]
})
export class CharacterlistPage implements OnInit {

  urlAPI: string = 'https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=02c250531a086a4ec9f85901e468bd06&hash=f111df853742da4151eb900421fdcc73';
  characters: any = [];
  offset: number = 0;
  limit: number = 20;
  
  searchTerm = '';
  filteredCharacters: any = [];

  searchInProgress?: Subscription;

  constructor(
    private http: HttpClient
  ) {}

  ngOnInit() {

    this.getCharacters(this.offset,this.limit);

  }

  getCharacters(offset: number, limit: number, newLoad: boolean = false){

    if(this.searchInProgress){
      this.searchInProgress.unsubscribe();
      this.searchInProgress = undefined;
    }

    if(newLoad){
      this.characters = [];
    }

    this.searchInProgress = this.http.get<any>(this.urlAPI + '&offset=' + offset + '&limit=' + limit).subscribe(res => {
      this.characters.push(...res.data.results);
      console.log(this.characters);
      this.searchInProgress = undefined;
    })

  }

  onIonInfinite(ev:any) {

    this.offset = this.offset + 20;

    this.getCharacters(this.offset, this.limit);

    setTimeout(() => {
      (ev as InfiniteScrollCustomEvent).target.complete();
    }, 500);

  }

  search(searchTerm: string){
    

    if(this.searchInProgress){
      this.searchInProgress.unsubscribe();
      this.searchInProgress = undefined;
    }

    if(this.searchTerm === ''){

      this.getCharacters(0, 20,true);

    }else{

      this.searchInProgress = this.http.get<any>(this.urlAPI + '&nameStartsWith=' + searchTerm).subscribe(res => {
      
        this.characters = res.data.results;
  
        this.searchInProgress = undefined;
      })

    }

  }

}
