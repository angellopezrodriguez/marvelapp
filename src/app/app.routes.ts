import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'home',
    loadComponent: () => import('./home/home.page').then((m) => m.HomePage),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'characterlist',
    loadComponent: () => import('./characterlist/characterlist.page').then( m => m.CharacterlistPage)
  },
  {
    path: 'characterprofile/:id',
    loadComponent: () => import('./characterprofile/characterprofile.page').then( m => m.CharacterprofilePage)
  },
];
